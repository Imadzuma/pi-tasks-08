#pragma once
#include <iostream>
#include <fstream>
using namespace std;
class Matrix
{
	//������������� �������� �����-������
	friend istream& operator >> (istream& stream, Matrix& matrix);
	friend ostream& operator << (ostream& stream, const Matrix& matrix);
private:
	int n;
	int m;
	double** matrix;
public:
	//����������� � �����������
	Matrix(int n, int m, double** matrix);
	//����������� �� ���������
	Matrix();
	//����������� �����������
	Matrix(const Matrix& Copy_matrix);
	//�������
	int GetN() const;
	int GetM() const;
	//�������� ��������
	Matrix operator + (const Matrix& matrix);
	//�������� ���������
	Matrix operator - (const Matrix& matrix);
	//�������� ���������
	Matrix operator * (const Matrix& matrix);
	//�������� ��� ������������� �����
	Matrix operator + (double d);
	Matrix operator - (double d);
	Matrix operator * (double d);
	Matrix operator / (double d);
	//�������� ����������
	Matrix& operator=(const Matrix& matrix_B);
	//����������� ��������
	Matrix& operator+=(const Matrix& matrix_B);
	Matrix& operator-=(const Matrix& matrix_B);
	Matrix& operator+=(double d);
	Matrix& operator-=(double d);
	Matrix& operator*=(double d);
	Matrix& operator/=(double d);
	//�������� ���������
	bool operator == (const Matrix& matrix_B) const;
	//�������� �����������
	bool operator != (const Matrix& matrix_B) const;
	//�������� ������� � ��������� �������
	double*& operator[] (int i) const;
	//�����
	Matrix Minor(int ii, int jj) const;
	//������������
	double Determinant() const;
	//�������� �������
	Matrix Reverse_Matrix() const;
	//����������������� �������
	Matrix Transpose() const;
	//������������ �������������
	void Randomize(int n, int m);
	//����������
	~Matrix();
};

