#include <iostream>
#include <fstream>
#include <vector>
#include "Matrix.h"
using namespace std;
vector<Matrix> Input(string file) {
	ifstream fcin(file);
	int size;
	fcin >> size;
	vector<Matrix> matrix(size);
	for(int i=0; i<size; ++i) 
		fcin >> matrix[i];
	return matrix;
}
void Output(string file, vector<Matrix> matrix) {
	ofstream fcout(file);
	fcout << matrix.size() << "\n";
	for (int i = 0; i < matrix.size(); ++i)
		fcout << matrix[i];
}
int main() {
	setlocale(LC_ALL, "Russian");
	vector<Matrix> matrix = Input("matrix.txt");
	matrix.resize(11);
	matrix[2].Randomize(4, 2);
	matrix[3] = matrix[0] + matrix[2];
	matrix[3] = matrix[0] + matrix[1];
	matrix[4] = matrix[0] - matrix[2];
	matrix[4] = matrix[0] - matrix[1];
	matrix[5] = matrix[0] * matrix[2];
	matrix[5] = matrix[0] * matrix[1];
	matrix[6] = matrix[2] + 3;
	matrix[7] = matrix[2] - 3;
	matrix[8] = matrix[2] * 3;
	matrix[9] = matrix[2] / 3;
	matrix[3] -= matrix[1];
	matrix[4] += matrix[1];
	matrix[6] += 1;
	matrix[7] -= 4;
	matrix[8] *= 3;
	matrix[9] /= 2;
	if (matrix[0] == matrix[3])
		cout << "True\n";
	else
		cout << "False\n";
	if (matrix[0]==matrix[5])
		cout << "True\n";
	else
		cout << "False\n";
	if (matrix[0] != matrix[4])
		cout << "True\n";
	else
		cout << "False\n";
	if (matrix[1] != matrix[4])
		cout << "True\n";
	else
		cout << "False\n";
	cout << "Determinant=" << matrix[0].Determinant() << "\n";
	matrix[10] = matrix[0].Reverse_Matrix();
	Output("new_matrix.txt", matrix);
	return 0;
}