#include "Matrix.h"

//����������� � �����������
Matrix::Matrix(int n, int m, double** matrix) {
	this->n = n;
	this->m = m;
	this->matrix = new double*[n];
	for (int i = 0; i < m; ++i) {
		this->matrix[i] = new double[m];
		for (int j = 0; j < m; ++j)
			this->matrix[i][j] = matrix[i][j];
	}
}
//����������� �� ���������
Matrix::Matrix() {
	n = 0;
	m = 0;
	matrix = nullptr;
}
//����������� �����������
Matrix::Matrix(const Matrix & Copy_matrix) {
	n = Copy_matrix.GetN();
	m = Copy_matrix.GetM();
	matrix = new double*[n];
	for (int i = 0; i < n; ++i) {
		matrix[i] = new double[m];
		for (int j = 0; j < m; ++j)
			matrix[i][j] = Copy_matrix[i][j];
	}
}
//�������
int Matrix::GetN() const {
	return n;
}
int Matrix::GetM() const {
	return m;
}
//�������� ��������
Matrix Matrix::operator+(const Matrix & matrix_B) {
	Matrix matrix_A(*this);
	if (matrix_A.GetM() != matrix_B.GetM() || matrix_A.GetN() != matrix_B.GetN()) {
		cout << "������ �������! ������� ������ �� ���������!\n";
		return Matrix();
	}
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] += matrix_B[i][j];
	}
	return matrix_A;
}
//�������� ���������
Matrix Matrix::operator-(const Matrix & matrix_B) {
	Matrix matrix_A(*this);
	if (matrix_A.GetM() != matrix_B.GetM() || matrix_A.GetN() != matrix_B.GetN()) {
		cout << "������ �������! ������� ������ �� ���������!\n";
		return Matrix();
	}
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] -= matrix_B[i][j];
	}
	return matrix_A;
}
//�������� ���������
Matrix Matrix::operator*(const Matrix & matrix_B)
{
	const Matrix & matrix_A = *this;
	if (matrix_A.GetM() != matrix_B.GetN()) {
		cout << "������ ��������! ������ ������ ������� �� ����� ������ ������!\n";
		return Matrix();
	}
	double** sqrt_array = new double*[matrix_A.GetN()];
	for (int i = 0; i < n; ++i) {
		sqrt_array[i] = new double[matrix_B.GetM()];
		for (int j = 0; j < matrix_B.GetM(); ++j) {
			sqrt_array[i][j] = 0;
			for(int k=0; k<m; ++k)
				sqrt_array[i][j] += matrix_A[i][k]* matrix_B[k][j];
		}
	}
	Matrix tmp(matrix_A.GetN(), matrix_B.GetM(), sqrt_array);
	return tmp;
}
//�������� ��� ������������� �����
Matrix Matrix::operator+(double d) {
	Matrix tmp(*this);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			tmp[i][j] += d;
	}
	return tmp;
}
Matrix Matrix::operator-(double d) {
	Matrix tmp = (*this);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			tmp[i][j] -= d;
	}
	return tmp;
}
Matrix Matrix::operator*(double d) {
	Matrix tmp = (*this);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			tmp[i][j] *= d;
	}
	return tmp;
}
Matrix Matrix::operator/(double d) {
	Matrix tmp = (*this);
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			tmp[i][j] /= d;
	}
	return tmp;
}
//�������� ����������
Matrix & Matrix::operator=(const Matrix & matrix_B) {
	n = matrix_B.GetN();
	m = matrix_B.GetM();
	matrix = new double*[n];
	for (int i = 0; i < n; ++i) {
		matrix[i] = new double[m];
		for (int j = 0; j < m; ++j)
			matrix[i][j] = matrix_B[i][j];
	}
	return *this;
}
//����������� ��������
Matrix & Matrix::operator+=(const Matrix & matrix_B) {
	Matrix& matrix_A = *this;
	if (matrix_A.GetM() != matrix_B.GetM() || matrix_A.GetN() != matrix_B.GetN()) {
		cout << "������ �������! ������� ������ �� ���������!\n";
		return matrix_A;
	}
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] += matrix_B[i][j];
	}
	return matrix_A;
}
Matrix & Matrix::operator-=(const Matrix & matrix_B)
{
	Matrix& matrix_A = *this;
	if (matrix_A.GetM() != matrix_B.GetM() || matrix_A.GetN() != matrix_B.GetN()) {
		cout << "������ �������! ������� ������ �� ���������!\n";
		return matrix_A;
	}
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] -= matrix_B[i][j];
	}
	return matrix_A;
}
Matrix & Matrix::operator+=(double d)
{
	Matrix& matrix_A = *this;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] += d;
	}
	return matrix_A;
}
Matrix & Matrix::operator-=(double d) {
	Matrix& matrix_A = *this;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] -= d;
	}
	return matrix_A;
}
Matrix & Matrix::operator*=(double d) {
	Matrix& matrix_A = *this;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] *= d;
	}
	return matrix_A;
}
Matrix & Matrix::operator/=(double d) {
	Matrix& matrix_A = *this;
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix_A[i][j] /= d;
	}
	return matrix_A;
}
//�������� ���������
bool Matrix::operator==(const Matrix & matrix_B) const {
	const Matrix& matrix_A = *this;
	if (matrix_A.GetN() != matrix_B.GetN() || matrix_A.GetM() != matrix_B.GetM())
		return false;
	for (int i = 0; i < matrix_A.GetN(); ++i) {
		for (int j = 0; j < matrix_A.GetM(); ++j) {
			if (matrix_A[i][j] != matrix_B[i][j])
				return false;
		}
	}
	return true;
}
//�������� �����������
bool Matrix::operator!=(const Matrix & matrix_B) const {
	const Matrix & matrix_A = *this;
	return !(matrix_A == matrix_B);
}
//�������� ������� � ��������� �������
double*& Matrix::operator[] (int i) const
{
	return matrix[i];
}
//�����
Matrix Matrix::Minor(int ii, int jj) const {
	double**minor = new double*[n - 1];
	for (int i = 0; i < n - 1; ++i) {
		minor[i] = new double[m - 1];
		for (int j = 0; j < m - 1; ++j)
			minor[i][j] = matrix[i + ((i < ii) ? 0 : 1)][j + ((j < jj) ? 0 : 1)];
	}
	Matrix matrix_minor(n - 1, m - 1, minor);
	return matrix_minor;
}
//������������
double Matrix::Determinant() const
{
	if (n != m) {
		cout << "������� �� ����������! ������������ ����� ����������!\n";
		return 0;
	}
	if (n == 1)
		return matrix[0][0];
	double determinant = 0;
	for (int k = 0; k < m; ++k) 
		determinant += pow(-1, k)*matrix[0][k] * this->Minor(0, k).Determinant();
	return determinant;
}
//�������� �������
Matrix Matrix::Reverse_Matrix() const {
	if (n != m) {
		cout << "������! �������� ������� ���� ������ � ������������!\n";
		return Matrix();
	}
	double determinant = this->Determinant();
	if (determinant == 0) {
		cout << "������! ������������ ������� ����� 0! ���������� ����� ��������!\n";
		return Matrix();
	}
	double** ac_matrix = new double*[n];
	for (int i=0; i<n; ++i)
		ac_matrix[i] = new double[m];
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			ac_matrix[j][i] = pow(-1, i + j) * this->Minor(i, j).Determinant();
	}
	Matrix union_matrix(n, m, ac_matrix);
	Matrix transpose_matrix(n, m, ac_matrix);
	return (transpose_matrix / determinant);
}
//����������������� �������
Matrix Matrix::Transpose() const
{
	double** new_matrix = new double*[m];
	for (int j = 0; j < m; ++j) {
		new_matrix[j] = new double[n];
		for (int i = 0; i < n; ++i)
			new_matrix[j][i] = matrix[i][j];
	}
	Matrix tmp(m, n, new_matrix);
	return tmp;
}
//������������ �������������
void Matrix::Randomize(int n, int m) {
	this->n = n;
	this->m = m;
	matrix = new double*[n];
	for (int i = 0; i < n; ++i)
		matrix[i] = new double[m];
	for (int i = 0; i < n; ++i) {
		for (int j = 0; j < m; ++j)
			matrix[i][j] = rand() % 10;
	}
}
//����������
Matrix::~Matrix() {
	for (int i = 0; i < n; ++i)
		delete matrix[i];
	delete[] matrix;
}
//�������� ����� �� ������
istream& operator >> (istream& stream, Matrix& matrix) {
	int n;
	int m;
	stream >> n >> m;
	double** sqrt_array = new double*[n];
	for (int i = 0; i < n; ++i) {
		sqrt_array[i] = new double[m];
		for (int j = 0; j < m; ++j)
			stream >> sqrt_array[i][j];
	}
	matrix = *(new Matrix(n, m, sqrt_array));
	return stream;
}
//�������� ������ �� ������
ostream& operator << (ostream& stream, const Matrix& matrix) {
	stream << matrix.GetN() << " " << matrix.GetM() << "\n";
	for (int i = 0; i < matrix.GetN(); ++i) {
		for (int j = 0; j < matrix.GetM(); ++j)
			stream << matrix[i][j] << " ";
		stream << "\n";
	}
	return stream;
}
